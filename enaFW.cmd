REM Créé par Titouan "LUX" ALLAIN à l'ISEN BREST le 14/12/18
REM MAJ 1.1 par Titouan "LUX" ALLAIN à l'ISEN BREST le 09/01/19 pour VirtualBox
netsh advfirewall set privateprofile state on
netsh advfirewall set domainprofile state on
netsh advfirewall set publicprofile state on
netsh interface ipv4 set address name="Ethernet" source=dhcp
netsh interface ipv4 set address name="VirtualBox Host-Only Network" source=dhcp
netsh int set int name="Wi-Fi" admin=enable