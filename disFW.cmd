REM Crée par Titouan "LUX" ALLAIN à l'ISEN BREST le 14/12/18
REM MAJ 1.1 par Titouan "LUX" ALLAIN à l'ISEN BREST le 09/01/19 pour VirtualBox
netsh advfirewall set privateprofile state off
netsh advfirewall set domainprofile state off
netsh advfirewall set publicprofile state off
set /p ip=Adresse Ipv4 :
set /p masque=Masque Ipv4 :
set /p ipVM=Adresse Ipv4 VM :
set /p masqueVM=Masque Ipv4 VM :
netsh interface ipv4 set address name="Ethernet" static %ip% %masque%
netsh interface ipv4 set address name="VirtualBox Host-Only Network" static %ipVM% %masqueVM%
netsh int set int name="Wi-Fi" admin=disable