# WinFW

**Version 1.1**

Simple scripts for Windows using CMD.

Each script is used to :

- Enable/disable the firewall of the computer
- Set/reset custom IPv4 address for Ethernet interface
- Set/reset custom IPv4 address for VirtualBox VM interface